import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Box from "@mui/material/Box";
import CloseIcon from "@mui/icons-material/Close";

const inter = Inter({ subsets: ["latin"] });

export async function getStaticProps() {
  const url = "https://restcountries.com/v3.1/all";
  const result = await fetch(url);
  return {
    props: {
      result: await result.json(),
    },
  };
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function Home({ result }) {
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);

  function dateTime(time) {
    let offset1 = time.split("C");
    let offset2 = offset1[1].split(":");
    let offset3 = parseInt(offset2[0]);
    let offset4 = parseInt(offset2[1]);
    let offset = offset3 + offset4 / 60;
    let date = new Date();
    let month = date.getMonth();
    let months = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let day = date.getDate();
    let year = date.getFullYear();
    let str = "";
    let str1 = "";
    let str2 = "";
    str = day + " " + months[month] + " " + year;
    let localTime = date.getTime();
    let localOffset = date.getTimezoneOffset() * 60000;
    let utc = localTime + localOffset;
    let x = new Date(utc + 3600000 * offset);
    let h = x.getHours();
    let m = x.getMinutes();
    let h1 = "";
    let m1 = "";
    if (h < 10) {
      h1 += "0" + h.toString();
    }
    if (m < 10) {
      m1 += "0" + m.toString();
    }

    if (h < 10) {
      if (m < 10) {
        str1 = h1 + ":" + m1;
      } else {
        str1 = h1 + ":" + m;
      }
    } else {
      if (m < 10) {
        str1 = h + ":" + m1;
      } else {
        str1 = h + ":" + m;
      }
    }
    str2 = str + " and " + str1;

    return str2;
  }

  let name;
  let flag;
  let timezone;
  let area;
  let id = 0;
  let population;
  const rows = [];

  result.map((element) => {
    id = id + 1;
    name = element.name.common;
    flag = element.flag;
    timezone = dateTime(element.timezones[0]);
    population = element.population;
    area = element.area;
    rows.push({ id, name, flag, timezone, population, area });
  });

  return (
    <>
      <Head>
        <title>Country List</title>
        <meta name="description" content="List of Countries with  their Details" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/flag.jpg" />
      </Head>
      <TableContainer>
        <Table sx={{ minWidth: 700 }}>
          <TableHead>
            <TableRow>
              <StyledTableCell>ID</StyledTableCell>
              <StyledTableCell>Country Name</StyledTableCell>
              <StyledTableCell align="right">Flag</StyledTableCell>
              <StyledTableCell align="right">Timezone</StyledTableCell>
              <StyledTableCell align="right">Population</StyledTableCell>
              <StyledTableCell align="right">Area</StyledTableCell>
              <StyledTableCell align="right">Action</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow key={row.id}>
                <StyledTableCell component="th" scope="row">
                  {row.id}{" "}
                </StyledTableCell>
                <StyledTableCell component="th" scope="row">
                  {row.name}
                </StyledTableCell>
                <StyledTableCell align="right">{row.flag}</StyledTableCell>
                <StyledTableCell align="right">{row.timezone}</StyledTableCell>
                <StyledTableCell align="right">
                  {row.population}
                </StyledTableCell>
                <StyledTableCell align="right">{row.area}</StyledTableCell>
                <StyledTableCell align="right">
                  <IconButton
                    onClick={() => {
                      setOpen(true);
                    }}
                  >
                    <EditIcon sx={{ fontSize: 23 }} />
                  </IconButton>

                  <IconButton
                    onClick={() => {
                      setOpen2(true);
                    }}
                  >
                    <DeleteIcon sx={{ fontSize: 23 }} />
                  </IconButton>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div>
        <Dialog
          className="popup"
          open={open}
          onClose={() => {
            setOpen(false);
          }}
        >
          <div className="cross">
            <Box position="absolute" top={0} right={0}>
              <IconButton
                onClick={() => {
                  setOpen(false);
                }}
              >
                <CloseIcon />
              </IconButton>
            </Box>
          </div>

          <DialogTitle className="heading">
            Are You Sure You Want to Edit This Activity?
          </DialogTitle>
          <DialogActions>
            <div
              className="yes"
              role="button"
              onClick={() => {
                setOpen(false);
              }}
            >
              YES
            </div>
            <div
              className="no"
              role="button"
              onClick={() => {
                setOpen(false);
              }}
              autoFocus
            >
              NO
            </div>
          </DialogActions>
        </Dialog>
      </div>

      <div>
        <Dialog
          open={open2}
          onClose={() => {
            setSOpen2(false);
          }}
        >
          <div className="cross">
            <Box position="absolute" top={0} right={0}>
              <IconButton
                onClick={() => {
                  setOpen2(false);
                }}
              >
                <CloseIcon />
              </IconButton>
            </Box>
          </div>

          <DialogTitle className="heading">
            Are You Sure You Want to Delete This Activity?
          </DialogTitle>
          <DialogActions>
            <div
              className="yes"
              role="button"
              onClick={() => {
                setOpen2(false);
              }}
            >
              YES
            </div>
            <div
              className="no"
              role="button"
              onClick={() => {
                setOpen2(false);
              }}
              autoFocus
            >
              NO
            </div>
          </DialogActions>
        </Dialog>
      </div>
    </>
  );
}
